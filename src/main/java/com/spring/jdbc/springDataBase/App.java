package com.spring.jdbc.springDataBase;
import java.util.List;

import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;
import org.springframework.jdbc.core.JdbcTemplate;

import com.spring.jdbc.springDataBase.dao.StudentDao;
import com.spring.jdbc.springDataBase.entities.Student;

public class App 
{
    public static void main( String[] args )
    {
        //spring jdbc
    	
    	//ClassPathXmlApplicationContext  It is used to load bean configuration xml file from the class path location of the application.
    	ApplicationContext  context=new ClassPathXmlApplicationContext("com/spring/jdbc/springDataBase/config.xml");
    
    	 //storing implemataion class in interface that is studentdao
    		StudentDao studentDao=context.getBean("studentdaoimpl",StudentDao.class);
    		Student student=new Student();
//    		student.setId(6);
//    		student.setName("safS");
//    		student.setCity("fsf");
    		
    		//kept this on dao class
    		
//    		int result =studentDao.insert(student);
//    		System.out.println("student added :   "+result);
//   
    
    
    
//    Student student=new Student();
//student.setId(4);
//	student.setName("divya");	
//	student.setCity("manglr");	
//	int result =studentDao.change(student);
//System.out.println("student data changed :   "+result);
    	
    	
    					//<---------------Result set Extarctor------------------>
    	
//    		List<Student> list=studentDao.findAll();  
//            
//    	    for(Student s:list)  
//    	        System.out.println(s);  
    		
    				//<---------------Row Mapper----------------------------->
    		List<Student> list=studentDao.findUsingRowMapper();  
          
  	    for(Student s:list)  
 	        System.out.println(s); 
    		
    	
    }
    
}
