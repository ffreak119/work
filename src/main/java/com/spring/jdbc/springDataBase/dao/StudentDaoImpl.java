package com.spring.jdbc.springDataBase.dao;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import org.springframework.dao.DataAccessException;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.ResultSetExtractor;
import org.springframework.jdbc.core.RowMapper;

import com.spring.jdbc.springDataBase.entities.Student;

public class StudentDaoImpl implements StudentDao{

	private JdbcTemplate jdbcTemplate;
	@Override
	public int insert(Student student) {
		
		
		//insert query														//we can insert with named parameter also
		String query="insert into student(id,name,city) values(?,?,?)";  // insert into employee values (:id,:name,:salary)  
		int result=this.jdbcTemplate.update(query,student.getId(),student.getName(),student.getCity());
		return result;
	}
	
	
	//update query
	
	@Override

	public int change(Student student) {
		String query="update student set name=?, city=? where id=?";
		int result1=this.jdbcTemplate.update(query,student.getId(),student.getName(),student.getCity());
		return result1;
	}

	public JdbcTemplate getJdbcTemplate() {
		return jdbcTemplate;
	}
	public void setJdbcTemplate(JdbcTemplate jdbcTemplate) {
		this.jdbcTemplate = jdbcTemplate;
	}
	
	
	//ResultSetExtractor
	@Override
	public List<Student> findAll() {
		return jdbcTemplate.query("select * from student",new ResultSetExtractor<List<Student>>(){

			@Override
			public List<Student> extractData(ResultSet rs) throws SQLException, DataAccessException {
				  List<Student> list=new ArrayList<Student>();
				while(rs.next()){  
			        Student s=new Student();  						
			        s.setId(rs.getInt(1));  
			        s.setName(rs.getString(2));  
			        s.setCity(rs.getString(3));  
			        list.add(s);  
			        }  
				return list;
			}
			});
	
	}
	
	
	//RowMapper
	@Override
	public List<Student> findUsingRowMapper() {
		 return jdbcTemplate.query("select * from student where id='2'",new RowMapper<Student>(){  
			    @Override  
			    public Student mapRow(ResultSet rs, int rownumber) throws SQLException {  
			        Student s=new Student();  
			        s.setId(rs.getInt(1));  
			        s.setName(rs.getString(2));  
			        s.setCity(rs.getString(3));  
			        return s;  
			    }  
			    });  
	}
	
	
	
	}
	


