package com.spring.jdbc.springDataBase.dao;

import java.util.List;

import com.spring.jdbc.springDataBase.entities.Student;

public interface StudentDao {
public int insert(Student student);
public int change(Student student);
List<Student> findAll();
List<Student> findUsingRowMapper();


}
